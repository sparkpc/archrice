(menu-bar-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

;; Use-Package
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

(package-refresh-contents)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
  (eval-when-compile
    (require 'use-package))



(setq use-package-always-ensure t)
(use-package evil
  :init
  (evil-mode 1))


;; Theme
(use-package challenger-deep-theme)
(load-theme 'challenger-deep t)

;; Fonts
(set-face-attribute 'default nil :font "Fira Code" :weight 'medium)
;; Mode Line
(use-package doom-modeline
  :hook (after-init . doom-modeline-mode))

(use-package all-the-icons)

;; Dashboard
(use-package dashboard)
(dashboard-setup-startup-hook)

;; org

(setq org-startup-indented t)
(use-package org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
